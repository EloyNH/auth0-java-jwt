package com.delivery.service;

import com.delivery.entity.Client;

public interface ClientService {
	
	public Client findByClientUser(String clientUser);
	
	public Client save(Client client);

}
