package com.delivery.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "client")
public class Client implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long clientId;

	@Column(length = 60)
	private String clientName;

	@Column(length = 12)
	private String clientPhoneNumber;

	@Column(length = 30)
	private String clientEmail;

	@Column(length = 15)
	private String clientDocumentType;

	@Column(length = 11)
	private String clientDocumentNumber;

	@Column(length = 15)
	private String clientUser;

	@Column(length = 254)
	private String clientPassword;

//    private boolean clientFacebook;
//    private boolean clientGoogle;
//    private boolean clientPhone;
	private String clientPhoto;// add by eloy

	@Column(length = 10)
	private String clientAuthentication;

	@Column(length = 30)
	private String clientUid;

	public Client() {

	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getClientPhoneNumber() {
		return clientPhoneNumber;
	}

	public void setClientPhoneNumber(String clientPhoneNumber) {
		this.clientPhoneNumber = clientPhoneNumber;
	}

	public String getClientEmail() {
		return clientEmail;
	}

	public void setClientEmail(String clientEmail) {
		this.clientEmail = clientEmail;
	}

	public String getClientDocumentType() {
		return clientDocumentType;
	}

	public void setClientDocumentType(String clientDocumentType) {
		this.clientDocumentType = clientDocumentType;
	}

	public String getClientDocumentNumber() {
		return clientDocumentNumber;
	}

	public void setClientDocumentNumber(String clientDocumentNumber) {
		this.clientDocumentNumber = clientDocumentNumber;
	}

	public String getClientUser() {
		return clientUser;
	}

	public void setClientUser(String clientUser) {
		this.clientUser = clientUser;
	}

	public String getClientPassword() {
		return clientPassword;
	}

	public void setClientPassword(String clientPassword) {
		this.clientPassword = clientPassword;
	}

	public String getClientPhoto() {
		return clientPhoto;
	}

	public void setClientPhoto(String clientPhoto) {
		this.clientPhoto = clientPhoto;
	}

	public String getClientAuthentication() {
		return clientAuthentication;
	}

	public void setClientAuthentication(String clientAuthentication) {
		this.clientAuthentication = clientAuthentication;
	}

	public String getClientUid() {
		return clientUid;
	}

	public void setClientUid(String clientUid) {
		this.clientUid = clientUid;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
