package com.delivery.jwt;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class JwtAuthorizationFilter extends BasicAuthenticationFilter{
	
	public static final String AUTHORIZATION = "Authorization";

	
	@Autowired
	private JwtService jwTservice;
	
	public JwtAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		String authHeader = request.getHeader(AUTHORIZATION);

		if (jwTservice.isBearer(authHeader)) {
			List<GrantedAuthority> authorities = jwTservice.roles(authHeader).stream().map(role -> new SimpleGrantedAuthority(role)).collect(Collectors.toList());
			
			UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(jwTservice.user(authHeader), null, authorities);
			SecurityContextHolder.getContext().setAuthentication(authenticationToken);
		}
		
		chain.doFilter(request, response);
		
		}
	
	
	
	

}
