package com.delivery.jwt;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.delivery.entity.Client;
import com.delivery.service.ClientService;


@RestController
@RequestMapping("/login")
public class JwtController {
	
	@Autowired
	private JwtService jwtService;
	
	@Autowired
	private ClientService clientService;
	
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private UserDetailsService userDetailsService;

	
//	@PreAuthorize("authenticated")
	@PostMapping
	public String login(@RequestBody Client client) throws Exception{
		
		Client clienteBD = null;
		//En primer lugar guardamos el cliente
		Client client2 = client;
		client2.setClientPassword(bCryptPasswordEncoder.encode(client2.getClientPassword()));
		clienteBD = clientService.save(client2);
		
		
		
		//Procedemos a authenticar con el cliente previamente guardado
//        try {
//            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(client.getClientUser(), client.getClientPassword());
//            authenticationManager.authenticate(authentication);
//        } catch (BadCredentialsException e) {
//            throw new Exception("Invalid username or password", e);
//        }
//        UserDetails userDetails = userDetailsService.loadUserByUsername(client.getClientUser());
//        
        List<String> roles = new ArrayList<>();
        roles.add("ROLE_USER");
        
        String token = jwtService.CrearToken(clienteBD.getClientUser(), roles);
        return token;
	}
	
}
