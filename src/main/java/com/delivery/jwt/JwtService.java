package com.delivery.jwt;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

@Service
public class JwtService {

	public static final String BEARER = "Bearer ";
	
	private static final String USER = "user";
	private static final String ROLES = "roles";
	private static final String ISSUER = "Delivery Soft";
	private static final int EXPIRES_IN_MILLISECONDS = 360000;
	private static final String SECRET = "clave-secreta-123";
	
	public String CrearToken(String user, List<String> roles){
		
		return JWT.create()
				.withIssuer(ISSUER)
				.withIssuedAt(new Date())
				.withNotBefore(new Date())
				.withExpiresAt(new Date(System.currentTimeMillis() + EXPIRES_IN_MILLISECONDS))
				.withClaim(USER, user)
				.withArrayClaim(ROLES, roles.toArray(new String[0]))
				.sign(Algorithm.HMAC256(SECRET));
				
	}

	public boolean isBearer(String authorization) {	
		
		return authorization != null && authorization.startsWith(BEARER) && authorization.split("\\.").length == 3;
	}

	public String user(String authorization) {
		return this.verificar(authorization).getClaim(USER).asString();
	}

	private DecodedJWT verificar(String authorization) throws JWTDecodeException, JWTVerificationException {
		if (!this.isBearer(authorization)) {
			throw new JWTDecodeException("No es bearer");
		}
		try {
			return JWT.require(Algorithm.HMAC256(SECRET)).withIssuer(ISSUER).build()
					.verify(authorization.substring(BEARER.length()));
		} catch (Exception e) {
			throw new JWTVerificationException("JWT no valido" + e.getMessage());
		}
	}

	public List<String> roles(String authorization) {
		return Arrays.asList(this.verificar(authorization).getClaim(ROLES).asArray(String.class));
	}
}
