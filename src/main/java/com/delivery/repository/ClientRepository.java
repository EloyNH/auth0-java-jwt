package com.delivery.repository;

import org.springframework.data.repository.CrudRepository;
import com.delivery.entity.Client;

public interface ClientRepository extends CrudRepository<Client, Long> {

	public Client findByClientUser(String clientUser);

}
