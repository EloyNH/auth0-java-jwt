package com.delivery;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Auth0JavaJwtApplicationTests {
	
	public static final String BEARER = "Bearer ";
	
	String token = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJuYmYiOjE1OTkyNjM1NzYsInJvbGVzIjpbIlJPTEVfVVNFUiJdLCJpc3MiOiJEZWxpdmVyeSBTb2Z0IiwiZXhwIjoxNTk5MjYzOTM2LCJpYXQiOjE1OTkyNjM1NzYsInVzZXIiOiJhZG1pbiJ9.u53ve4mrqLLjH-5x0GOap4yNHSbzN5FvlAUSPVdXqPU";

	@Test
	void contextLoads() {
		
		System.out.println(token.split("\\.").length);
		
		System.out.println(token.startsWith(BEARER));
		
		System.out.println(token.substring(BEARER.length()));
	}

}
